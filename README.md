# array-lang

This compares array performance across different language implementations. The array tests are based of the 'ion compiler' tests from bitwise (https://github.com/pervognsen/bitwise).

Note: The C "stretchy buffers" props go to Sean Barrett.

## Test Results with 1,000,000 items (late 2016 15" MacBook Pro)
 This test 1) inserts integers into an array one at a time and then 2) iterates over that array verfiying the integers are the expected values.
 
| Language | Array Type | Pass 1 (ms) | Pass 2 (ms) |
|-|-|:-:|:-:|
| C | "stretchy buffers" | 7 | 7 |
| C++ | std::vector | 12 | 4 |
| Objective-C | NSArray | 76 | 47 |
| Swift 4.1 | non-optional | 18 | 6 |
| Swift 4.1 | forced optional | 20 | 8 |
| Swift 4.1 | unforced optional | 20 | 9 |
